package org.wit.android.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import org.wit.mytweet.models.User;


public class DbHelper extends SQLiteOpenHelper {

    //fields for creation and use of sqlite database
    static final String TAG = "DbHelper";
    static final String DATABASE_NAME = "users.db";
    static final int DATABASE_VERSION = 1;
    static final String TABLE_USERS = "tableUsers";
    static final String PRIMARY_KEY = "id";
    static final String FIRSTNAME = "firstName";
    static final String LASTNAME = "lastName";
    static final String EMAIL = "email";
    static final String PASSWORD = "password";

    private static DbHelper sInstance;

    Context context;

    /**
     *
     * @param context
     */
    public DbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    /**
     * onCreate method taking db as param, creating table of users.
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable =
                "CREATE TABLE tableUsers " +
                        "(id text PRIMARY_KEY, " +
                        "firstName TEXT, " +
                        "lastName TEXT, " +
                        "email TEXT, " +
                        "password TEXT)";

        db.execSQL(createTable);
        Log.d(TAG, "DbHelper.onCreated: " + createTable);
    }


    /**
     * Method to insert user entries to table.
     *
     * @param user
     */
    public void addUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(PRIMARY_KEY, user.id.toString());
        values.put(FIRSTNAME, user.firstName);
        values.put(LASTNAME, user.lastName);
        values.put(EMAIL, user.email);
        values.put(PASSWORD, user.password);
        // Insert record
        db.insert(TABLE_USERS, null, values);
        db.close();
    }

    /**
     *Method called when app is upgraded, launched and the database is not the same.
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("drop table if exists " + TABLE_USERS);
        Log.d(TAG, "onUpdated");
        onCreate(db);
    }

    /**
     * Method for calling db in activities setting appropriate context.
     *
     * @param context
     * @return
     */
    public static synchronized DbHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DbHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    /**
     * Helper method to find user in database, utilized in login to ensure user credentials are
     * matching and correct.
     *
     * @param useremail
     * @param userpassword
     * @return boolean
     */
    public boolean findUser(String useremail, String userpassword) {
        SQLiteDatabase db = sInstance.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT email, password from tableUsers WHERE email = " +"'"+ useremail+"'"+" AND password = " + "'"+userpassword+"'", null);
        Log.d("whatttttttttttt", String.valueOf(cur));
        if(cur.getCount() == 1){
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    /**
     * Helper method to check if email exists in database when a user is registering, utilized in
     * signup.
     *
     * @param useremail
     * @return boolean
     */
    public boolean checkEmail(String useremail) {
        SQLiteDatabase db = sInstance.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT email from tableUsers WHERE email = " +"'"+ useremail+"'", null);
        Log.d("whatttttttttttt", String.valueOf(cur));
        if(cur.getCount() == 1){
            cur.close();
            return true;
        }
        cur.close();
        return false;
    }

    /**
     * Helper method to get signed in users name on login.
     *
     * @param email
     * @param password
     * @return String
     */
    public String userName(String email, String password) {
        SQLiteDatabase db = sInstance.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT firstName from tableUsers WHERE email = " +"'"+ email+"'"+" AND password = " + "'"+password+"'", null);
        String user = "";
        if(cur.moveToFirst()) {
            user = String.valueOf(cur.getString(0));
        }
        cur.close();
        return user;

    }
}




