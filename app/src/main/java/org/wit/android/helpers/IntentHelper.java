package org.wit.android.helpers;

import java.io.Serializable;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.NavUtils;

public class IntentHelper {

    /**
     * Method to start activity with additional data.
     *
     * @param parent
     * @param classname
     * @param extraID
     * @param extraData
     */
    public static void startActivityWithData (Activity parent, Class classname, String extraID, Serializable extraData) {
        Intent intent = new Intent(parent, classname);
        intent.putExtra(extraID, extraData);
        parent.startActivity(intent);
    }

    /**
     * Method to start intent by getting parent activity of activity from which it is accessed.
     *
     * @param parent
     */
    public static void navigateUp(Activity parent) {
        Intent upIntent = NavUtils.getParentActivityIntent(parent);
        NavUtils.navigateUpTo(parent, upIntent);
    }
}