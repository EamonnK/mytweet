package org.wit.mytweet.models;

import java.util.UUID;

public class User {
    //fields for use in model
    public UUID id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;

    /**
     * Constructor for objects of model user.
     *
     * @param firstName
     * @param lastName
     * @param email
     * @param password
     */
    public User(String firstName, String lastName, String email, String password){
        this.id        = UUID.randomUUID();
        this.firstName = firstName;
        this.lastName  = lastName;
        this.email     = email;
        this.password  = password;
    }
}
