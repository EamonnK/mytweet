package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import org.wit.android.helpers.DbHelper;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.User;
import org.wit.mytweet.R;

public class Signup extends AppCompatActivity {
    //fields for use in activity
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private MyTweetApp app;
    private DbHelper db;

    /**
     * Method to start appropriate layout, initializing variables on the page.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        app = (MyTweetApp) getApplication();
        firstName = (EditText) findViewById(R.id.firstName);
        firstName.setText("");
        lastName = (EditText) findViewById(R.id.lastName);
        lastName.setText("");
        email = (EditText) findViewById(R.id.email);
        email.setText("");
        password = (EditText) findViewById(R.id.password);
        db = DbHelper.getInstance(this);
    }

    /**
     * Method to register user, taking input variables, ensuring all fields are filled, checking
     * against the database to make sure email doesnt already exist, replying with appropriate toast
     * if email already exists.
     *
     * @param view
     */
    public void registerUser(View view){
        String firstName = this.firstName.getText().toString();
        String lastName  = this.lastName.getText().toString();
        String email     = this.email.getText().toString();
        String password  = this.password.getText().toString();
        if(db.checkEmail(email)){
            Toast toast = Toast.makeText(this, "Email is already in our database.", Toast.LENGTH_SHORT);
            this.email.setText("");
            toast.show();
        }
        else if(firstName.equals("") || lastName.equals("") || email.equals("") || password.equals("") ) {
            Toast toast = Toast.makeText(this, "All fields must be filled", Toast.LENGTH_SHORT);
            toast.show();
        }else{
            User user = new User(firstName, lastName, email, password);
            app.dbHelper.addUser(user);
            Log.v("User", "New user : " + user.email + " " + user.password);
            startActivity(new Intent(this, Login.class));
            finish();
        }
    }

    /**
     * Method to listen for pressing of the hardware back button.
     * if pressed calling onbackpressed().
     *
     * @param keyCode
     * @param event
     * @return boolean
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Method to redirect user to welcome activity when back button is pressed,
     * finishing the current activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, Welcome.class);
        startActivity(intent);
        finish();
    }
}
