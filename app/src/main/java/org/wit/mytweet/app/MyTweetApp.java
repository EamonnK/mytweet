package org.wit.mytweet.app;

import android.app.Application;
import android.util.Log;
import org.wit.android.helpers.DbHelper;
import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.models.TweetListSerializer;

public class MyTweetApp extends Application {
    //fields for use in application
    public TweetList tweetList;
    private static final String FILENAME = "tweetList.json";
    protected static MyTweetApp app;
    public String thisUser;
    public DbHelper dbHelper;
    static final String TAG = "MyTweetApp";

    /**
     * Method to initialize variables for use throughout the app.
     */
    @Override
    public void onCreate(){
        super.onCreate();
        dbHelper = new DbHelper(getApplicationContext());
        Log.d(TAG, "MyTweet launched");
        app = this;
        TweetListSerializer serializer = new TweetListSerializer(this, FILENAME);
        tweetList = new TweetList(serializer);
        Log.v("TweetPagerActivity", "TweetPagerActivity App Started");
    }

    /**
     * Getter for call in activities to return MyTweetApp
     *
     * @return MyTweetApp
     */
    public static MyTweetApp getApp(){
        return app;
    }

}
