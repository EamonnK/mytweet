package org.wit.mytweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static org.wit.android.helpers.ContactHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;

import org.wit.android.helpers.ContactHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;
import java.util.Date;

public class TweetActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener {
    //fields for use in activity
    private Button tweetButton;
    private Button emailButton;
    private Button contactButton;
    private TextView count;
    private EditText editText;
    private Tweet tweet;
    private TextView datetime;
    private Long date;
    private TweetList tweetList;
    private String emailAddress = "";
    private MyTweetApp app;

    private static final int REQUEST_CONTACT = 1;

    /**
     * Method to start appropriate layout, initializing variables on the page.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweetactivity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tweetButton = (Button) findViewById(R.id.tweetButton);
        tweetButton.setOnClickListener(this);
        emailButton = (Button) findViewById(R.id.emailButton);
        emailButton.setOnClickListener(this);
        contactButton = (Button) findViewById(R.id.contactButton);
        contactButton.setOnClickListener(this);
        count = (TextView) findViewById(R.id.count);
        editText = (EditText) findViewById(R.id.editText);
        editText.addTextChangedListener(this);
        datetime = (TextView) findViewById(R.id.date);
        date = new Date().getTime();
        datetime.setText(dateString());
        app = (MyTweetApp) getApplication();
        tweetList = app.tweetList;
    }

    /**
     * Helper method to format date string on view
     *
     * @return String
     */
    public String dateString() {
        String dateFormat = "EEE d MMM yyyy H:mm";
        return android.text.format.DateFormat.format(dateFormat, date).toString();
    }

    /**
     * onClick listener method for tweet, select contact and email buttons
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
             case R.id.tweetButton :
                 //method extracted due to size
                 checkTweet();
                 break;
            case R.id.contactButton :
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                break;
            case R.id.emailButton :
                sendEmail(this, emailAddress,"From Twitter", editText.getText().toString());
                contactButton.setText("Select Contact");
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    /**
     * Listnener method to detect text change, used to adjust counter text on tweetactivity
     *
     * @param s
     */
    @Override
    public void afterTextChanged(Editable s) {
        count.setText(String.valueOf(140 - s.length()));
    }

    /**
     * listener method for up button, utilizing navigateUp helper to return to parent.
     *
     * @param item
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:  navigateUp(this);
                break;
        }
        return true;
    }

    /**
     * Method to determine result of activity and handle based on resultcode, returning if OK
     * or setting variables for emailing in the case request_contact.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CONTACT:
                String name = ContactHelper.getContact(this, data);
                emailAddress = ContactHelper.getEmail(this, data);
                contactButton.setText(name + " : " + emailAddress);
                break;
        }

    }

    /**
     * Method called when creating new tweet in onClick, checking if tweet has content and dis-
     * carding if not, otherwise creating tweet, initializing fields and saving to tweetList
     * returning user to tweetlistactivity.
     *
     */
    public void checkTweet(){
        if(this.editText.getText().toString().equals("")){
            startActivity(new Intent(this, TweetListActivity.class));
            finish();
        }else{
            tweet = new Tweet();
            tweet.setTweetMessage(this.editText.getText().toString());
            tweet.user = app.thisUser;
            tweetList.addTweet(tweet);
            tweetList.saveTweets();
            Toast toast = Toast.makeText(this, "Tweet sent!", Toast.LENGTH_SHORT);
            toast.show();
            Intent intent = new Intent(this, TweetListActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
            finish();
        }
    }
}