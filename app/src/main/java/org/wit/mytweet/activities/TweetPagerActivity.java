package org.wit.mytweet.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;
import java.util.ArrayList;
import static org.wit.android.helpers.IntentHelper.navigateUp;

public class TweetPagerActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private ArrayList<Tweet> tweets;
    private TweetList tweetList;
    private PagerAdapter pagerAdapter;
    ActionBar actionBar;

    /**
     * Method to start viewpager, initializing variables for use.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewPager = new ViewPager(this);
        viewPager.setId(R.id.viewPager);
        setContentView(viewPager);
        setTweetList();
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tweets);
        viewPager.setAdapter(pagerAdapter);
        setCurrentItem();
        actionBar = getSupportActionBar();
    }

    /**
     * method to set current tweet item.
     */
    private void setCurrentItem() {
        Long tweetId = (Long) getIntent().getSerializableExtra(TweetFragment.EXTRA_TWEET_ID);
        for (int i = 0; i < tweets.size(); i++) {
            if (tweets.get(i).id.equals(tweetId)) {
                viewPager.setCurrentItem(i);
                break;
            }
        }
    }

    /**
     * Method to set list of tweets, called in oncreate.
     */
    private void setTweetList() {
        MyTweetApp app = (MyTweetApp) getApplication();
        tweetList = app.tweetList;
        tweets = tweetList.tweets;
    }



    class PagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Tweet>  tweets;

        /**
         * Constructor for pageradapter.
         *
         * @param fm
         * @param tweets
         */
        public PagerAdapter(FragmentManager fm, ArrayList<Tweet> tweets) {
            super(fm);
            this.tweets = tweets;
        }

        /**
         * Method to return size of tweets arraylist.
         *
         * @return int
         */
        @Override
        public int getCount()
        {
            return tweets.size();
        }

        /**
         * Method  to get tweet by position and return appropriately initialized fragment.
         * @param pos
         * @return Fragment
         */
        @Override
        public Fragment getItem(int pos) {
            Tweet tweet = tweets.get(pos);
            Bundle args = new Bundle();
            args.putSerializable(TweetFragment.EXTRA_TWEET_ID, tweet.id);
            TweetFragment fragment = new TweetFragment();
            fragment.setArguments(args);
            return fragment;
        }
    }

    /**
     * Method to listen for pressing of the hardware back button.
     * if pressed calling onbackpressed().
     *
     * @param keyCode
     * @param event
     * @return boolean
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Method to redirect user to welcome activity when back button is pressed,
     * navigating to parent activity.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        navigateUp(this);
    }
}