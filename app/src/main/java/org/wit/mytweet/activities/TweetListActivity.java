package org.wit.mytweet.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import org.wit.mytweet.R;


public class TweetListActivity extends AppCompatActivity {

    /**
     * onCreate method to initialize fragment container containing tweetlistfragment.
     *
     * @param savedInstanceState
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_container);

        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(R.id.fragmentContainer);
        if (fragment == null) {
            fragment = new TweetListFragment();
            manager.beginTransaction().add(R.id.fragmentContainer, fragment).commit();
        }
    }

    /**
     * Method to listen for pressing of the hardware back button.
     * if pressed calling onbackpressed().
     *
     * @param keyCode
     * @param event
     * @return boolean
     */
   @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Method to exit app when hardware back button is pressed.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        System.exit(0);
    }

}
