package org.wit.mytweet.models;

import java.util.Date;
import java.util.Random;
import org.json.JSONException;
import org.json.JSONObject;


public class Tweet {
    //fields for use in model
    public String tweetMessage;
    public Long date;
    public Long id;
    public String user;
    //fields specifically for json
    private static final String JSON_TWEETMESSAGE = "tweetMessage";
    private static final String JSON_DATE         = "date";
    private static final String JSON_ID           = "id";
    private static final String JSON_USER= "user";

    /**
     * Constructor for tweet objects.
     */
    public Tweet(){
        id = unsignedLong();
        date = new Date().getTime();
    }

    /**
     * Tweet constructor for json persistence.
     *
     * @param json
     * @throws JSONException
     */
    public Tweet(JSONObject json) throws JSONException {
        tweetMessage  = json.getString(JSON_TWEETMESSAGE);
        id            = json.getLong(JSON_ID);
        date          = json.getLong(JSON_DATE);
        user          = json.getString(JSON_USER);
    }


    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_TWEETMESSAGE  , tweetMessage);
        json.put(JSON_ID            , Long.toString(id));
        json.put(JSON_DATE          , date);
        json.put(JSON_USER          , user);
        return json;
    }

    /**
     * Method to set tweetmessage text.
     *
     * @param tweetMessage
     */
    public void setTweetMessage(String tweetMessage){
        this.tweetMessage = tweetMessage;
    }

    /**
     * Method to return date string
     * @return String
     */
    public String getDateString() {
        return dateString();
    }

    /**
     * Method to format date.
     *
     * @return String
     */
    public String dateString() {
        String dateFormat = "EEE d MMM yyyy H:mm";
        return android.text.format.DateFormat.format(dateFormat, date).toString();
    }

    /**
     * Method to generate random tweet id, only allowing positive returns.
     *
     * @return Long
     */
    private Long unsignedLong() {
        long rndVal = 0;
        do {
            rndVal = new Random().nextLong();
        } while (rndVal <= 0);
        return rndVal;
    }
}
