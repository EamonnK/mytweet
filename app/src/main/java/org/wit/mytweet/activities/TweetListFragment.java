package org.wit.mytweet.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AbsListView;
import android.view.ActionMode;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;
import org.wit.mytweet.settings.SettingsActivity;
import java.util.ArrayList;
import static org.wit.android.helpers.IntentHelper.startActivityWithData;

public class TweetListFragment extends ListFragment implements AdapterView.OnItemClickListener, AbsListView.MultiChoiceModeListener {
    //fields for use in activity
    private ArrayList<Tweet> tweets;
    private ListView listView;
    private TweetList tweetList;
    private TweetAdapter adapter;
    MyTweetApp app;

    /**
     * oncreate method to initialize variables.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.app_name);
        app = MyTweetApp.getApp();
        tweetList = app.tweetList;
        tweets = tweetList.tweets;
        adapter = new TweetAdapter(getActivity(), tweets);
        setListAdapter(adapter);
    }

    /**
     * Method to inflate view initializing listview and View v
     *
     * @param inflater
     * @param parent
     * @param savedInstanceState
     * @return View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        listView = (ListView) v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);
        return v;
    }

    /**
     * Listener method to get position of tweet in adapter and start activity with appropriate
     * data.
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Tweet tweet = adapter.getItem(position);
        startActivityWithData(getActivity(), TweetPagerActivity.class, "TWEET_ID", tweet.id);

    }

    /**
     * method to inflate options menu.
     *
     * @param menu
     * @param inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.tweetlist, menu);
    }

    /**
     * Listener method to determine which menu option has been selected and respond with
     * appropriate action.
     *
     * @param item
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_tweet:
                startActivity(new Intent(getActivity(), TweetActivity.class));
                break;

            case R.id.clear: tweetList.clearTweetList();
                startActivity(new Intent(getActivity(), TweetListActivity.class));
                getActivity().finish();
                break;

            case R.id.action_settings:
                startActivity(new Intent(getActivity(), SettingsActivity.class));
                return true;

            case R.id.welcome:
                startActivity(new Intent(getActivity(), Welcome.class));
        }
        return true;
    }

    @Override
    public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

    }

    /**
     * Method to inflate tweetlist delete menu.
     *
     * @param mode
     * @param menu
     * @return boolean
     */
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.tweet_list_context, menu);
        return true;
    }


    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    /**
     * Method to listen for item click, entering delte mode on long hold.
     *
     * @param mode
     * @param item
     * @return boolean
     */
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_delete_tweet:
                deleteTweet(mode);
                return true;
            default:
                return false;
        }
    }

    /**
     * Method called to delete selected items from tweetlist.
     *
     * @param actionMode
     */
    private void deleteTweet(ActionMode actionMode) {
        for (int i = adapter.getCount() - 1; i >= 0; i--) {
            if (listView.isItemChecked(i)) {
                tweetList.deleteTweet(adapter.getItem(i));
            }
        }
        actionMode.finish();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }

    /**
     * Listener for list item clicked, getting item by position, starting tweetpageractivity
     * and adding tweetFragment and tweetId extras.
     *
     * @param l
     * @param v
     * @param position
     * @param id
     */
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Tweet tweet = ((TweetAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), TweetPagerActivity.class);
        i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet.id);
        startActivityForResult(i, 0);
    }

    /**
     * Method to notify TweetAdapter of change in data-set on resumption of TweetListFragment
     */
    @Override
    public void onResume() {
        super.onResume();
        ((TweetAdapter)getListAdapter()).notifyDataSetChanged();
    }
}

class TweetAdapter extends ArrayAdapter<Tweet> {
    private Context context;

    /**
     * Constructor for tweetadapter
     *
     * @param context
     * @param tweets
     */
    public TweetAdapter(Context context, ArrayList<Tweet> tweets) {
        super(context, 0, tweets);
        this.context = context;
    }

    /**
     * Method to get tweet list items and initialize their fields, user, message and date.
     *
     * @param position
     * @param convertView
     * @param parent
     * @return convertView
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null){
            convertView = inflater.inflate(R.layout.list_item_tweet, null);
        }
        Tweet tweet = getItem(position);

        TextView tweet_list_user= (TextView) convertView.findViewById(R.id.tweet_list_user);
        tweet_list_user.setText(tweet.user + " tweets...");

        TextView tweet_list_item = (TextView) convertView.findViewById(R.id.tweet_list_item);
        tweet_list_item.setText(tweet.tweetMessage);

        TextView tweet_list_item_date = (TextView) convertView.findViewById(R.id.tweet_list_item_date);
        tweet_list_item_date.setText(tweet.getDateString());


        return convertView;
    }
}
