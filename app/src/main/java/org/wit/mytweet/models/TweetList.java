package org.wit.mytweet.models;

import android.util.Log;
import java.util.ArrayList;

public class TweetList {
    //fields for use in model
    public ArrayList<Tweet> tweets;
    private TweetListSerializer   serializer;

    /**
     * Constructor for tweetlist, taking serializer and attempting to load tweets, else creating new
     * arrayList.
     *
     * @param serializer
     */
    public TweetList(TweetListSerializer serializer) {
        this.serializer = serializer;
        try {
            tweets = serializer.loadTweets();
        }
        catch (Exception e) {
            tweets = new ArrayList<Tweet>();
        }
    }

    /**
     * Method to add tweets to existing arraylist.
     *
     * @param tweet
     */
    public void addTweet(Tweet tweet) {
        tweets.add(tweet);
    }

    /**
     * Method to delete all tweets from timeline, saving list on completion.
     */
    public void clearTweetList(){
        for(int i = tweets.size()-1; i >= 0; i--){
            tweets.remove(i);
        }
        saveTweets();
    }

    /**
     * Method to get tweet by tweet id.
     *
     * @param id
     * @return Tweet
     */
    public Tweet getTweet(Long id) {
        Log.i(this.getClass().getSimpleName(), "Long parameter id: " + id);

        for (Tweet tweet : tweets) {
            if (id.equals(tweet.id)) {
                return tweet;
            }
        }
        return null;
    }

    /**
     * method to save tweets, returning true if successful and false if not.
     *
     * @return boolean
     */
    public boolean saveTweets() {
        try {
            serializer.saveTweets(tweets);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Method called to remove selected tweets in Tweetlistfragment activity.
     *
     * @param tweet
     */
    public void deleteTweet(Tweet tweet){
        if(tweets.contains(tweet)) {
            tweets.remove(tweet);
        }
    }
}
