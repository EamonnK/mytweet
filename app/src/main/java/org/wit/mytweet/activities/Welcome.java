package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import org.wit.mytweet.R;

public class Welcome extends AppCompatActivity implements View.OnClickListener{
    //fields for use in activity
    private Button loginButton;
    private Button signupButton;

    /**
     * Method to start appropriate layout, initializing variables on the page.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        loginButton = (Button) findViewById(R.id.loginButton);
        loginButton.setOnClickListener(this);
        signupButton = (Button) findViewById(R.id.signupButton);
        signupButton.setOnClickListener(this);
    }

    /**
     * onClick listener for login and signup buttons, redirecting user to appropriate activity
     * once clicked.
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.loginButton : startActivity(new Intent(this, Login.class));
                finish();
                break;
            case R.id.signupButton : startActivity(new Intent(this, Signup.class));
                finish();
                break;
        }

    }
}

