package org.wit.mytweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import static org.wit.android.helpers.ContactHelper.sendEmail;
import static org.wit.android.helpers.IntentHelper.navigateUp;
import org.wit.android.helpers.ContactHelper;
import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.TweetList;


public class TweetFragment extends Fragment implements TextWatcher, View.OnClickListener {
    //fields for use in activity
    public static   final String  EXTRA_TWEET_ID = "mytweet.TWEET_ID";
    private Button tweetButton;
    private Button emailButton;
    private Button contactButton;
    private TextView count;
    private TextView tweeter;
    private EditText editText;
    private Tweet tweet;
    private TextView date;
    private TweetList tweetList;
    private static final int REQUEST_CONTACT = 1;
    private String emailAddress = "";
    MyTweetApp app;

    /**
     * oncreate method to initialize variables, including selected tweet.
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Long tweetId = (Long)getArguments().getSerializable(EXTRA_TWEET_ID);
        app = MyTweetApp.getApp();
        tweetList = app.tweetList;
        tweet = tweetList.getTweet(tweetId);
    }

    /**
     * Method to inflate view, utilizing addListneners and updateControls
     * @param inflater
     * @param parent
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);
        TweetPagerActivity myTweet = (TweetPagerActivity)getActivity();
        myTweet.actionBar.setDisplayHomeAsUpEnabled(true);
        addListeners(v);
        return v;
    }

    /**
     * Method to initialize variables, setting content where appropriate.
     *
     * @param v
     */
    private void addListeners(View v){
        tweetButton = (Button) v.findViewById(R.id.tweetButton);
        emailButton = (Button) v.findViewById(R.id.emailButton);
        emailButton.setOnClickListener(this);
        contactButton = (Button) v.findViewById(R.id.contactButton);
        contactButton.setOnClickListener(this);
        count = (TextView) v.findViewById(R.id.count);
        count.setText(String.valueOf(140 - tweet.tweetMessage.length()));
        editText = (EditText) v.findViewById(R.id.editText);
        editText.setText(tweet.tweetMessage);
        editText.setEnabled(false);
        date = (TextView) v.findViewById(R.id.date);
        date.setText(tweet.getDateString());
        tweeter = (TextView) v.findViewById(R.id.tweeter);
        tweeter.setText(tweet.user.toUpperCase()+" tweeted...");
    }


    /**
     * onClick listener method for select contact and email buttons
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.contactButton :
                Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                break;
            case R.id.emailButton :
                sendEmail(getActivity(), emailAddress,"From Twitter", editText.getText().toString());
                contactButton.setText("Select Contact");
                break;
        }
    }

    /**
     * Method to determine result of activity and handle based on resultcode, returning if OK
     * or setting variables for emailing in the case request_contact.
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case REQUEST_CONTACT:
                String name = ContactHelper.getContact(getActivity(), data);
                emailAddress = ContactHelper.getEmail(getActivity(), data);
                contactButton.setText(name + " : " + emailAddress);
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    /**
     * listener method for up button, utilizing navigateUp helper to return to parent.
     *
     * @param item
     * @return boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                navigateUp(getActivity());
                break;
        }
        return true;
    }
}